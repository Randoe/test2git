import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
public class BoatReader {
    public static void main(String[] args) throws IOException{
        List<Gear> rentals = new ArrayList<>();
        Path p = Paths.get("boatgear.csv");
        List<String> lines = Files.readAllLines(p);
        
        for(String line:lines){
            String[] pieces = line.split(",");

            if(pieces[0].equals("lifevest")){
                rentals.add(new LIfeVest(pieces[1], pieces[2], Integer.parseInt(pieces[3])));
            }
            if(pieces[0].equals("motorboat")){
                rentals.add(new MotorBoat(pieces[1], pieces[2], Integer.parseInt(pieces[3]), Double.parseDouble(pieces[4])));
            }
            if(pieces[0].equals("pedalboat")){
                rentals.add(new PedalBoat(pieces[1], pieces[2], Integer.parseInt(pieces[3])));
            }
        }

        for(int i=0; i<rentals.size(); i++){
            if(rentals.get(i) instanceof Boat){
                ((Boat) rentals.get(i)).pilot();
            }
        }
    }
}
