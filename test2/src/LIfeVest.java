import java.util.Scanner;
public class LIfeVest implements Gear{
    private int size;
    private String id;
    private String renter;

    public LIfeVest(String id, String renter, int size){
        this.size = size;
        this.renter = renter;
        this.id = id;
    }
    
    public void rentGear(String name) {
        Scanner reader = new Scanner(System.in);
        System.out.println("what's your vest size?");
        int answer = Integer.parseInt(reader.next());
        if(this.size == answer){
            this.renter = name;
        }
        reader.close();
    }

    
    public String getRenter() {
        return this.renter;
    }
    
}
