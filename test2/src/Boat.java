public abstract class Boat implements Gear{
    protected int passengers;
    protected String id;
    protected String renter;

    public Boat(int p, String id, String renter){
        this.passengers = p;
        this.id = id;
        this.renter = renter;
    }

    public void rentGear(String name){
        this.renter = name;
    }
    public String getRenter(){
        return this.renter;
    }

    public abstract void pilot();
}
