public class PedalBoat extends Boat{

    public PedalBoat(String id, String renter, int p) {
        super(p, id, renter);
    }
    
    public void pilot() {
        System.out.println("The pedal boat was pedalled!");
    }
    
}
