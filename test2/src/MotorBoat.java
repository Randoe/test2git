public class MotorBoat extends Boat {
    protected double horsePower;

    public MotorBoat(String id, String renter, int p, double horsePower) {
        super(p, id, renter);
        this.horsePower = horsePower;
    }

    public void pilot() {
        System.out.println("Vroom!");
    }
}
