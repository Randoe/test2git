public interface Gear {
    public void rentGear(String name);
    public String getRenter();
}
